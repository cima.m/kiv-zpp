# CI/CD v GitLabu

_Ing. Martin Šimek_

KIV/ZPP 25. 2. 2025

Jak formalizovat a automatizovat proces vývoje softwaru, 
aby se vývojář mohl soustředit na programování 
a bylo ponětí, co ve které verzi programu vlastně je.

## Obsah

- Motivace – proč verzovat
- Co je to CI/CD a proč ho mít
- Pipelines, Variables, Secrets
- Build reporty (unit testy, SW metriky)
- Deployment

## Motivace - verzování (Git)

- Centrální úložiště - zdroj pravdy
  - Všichni pracují na společném díle
- Historie změn (s popisem a metadaty)
  - Každý příspěvek nese stručný komentář co obsahuje a odkaz na detaily + *Autora*
- Verze programu
  - konkrétní bod v historii vývoje ze kterého se vydala verze 1.0.0, 1.0.1, ...
- *Větve* - Paralelní historie
  - Příspěvek trvá více dní než se má přidat do hlavního proudu
  - Vydané verze potřebují opravná vyrání (patch, hotfix, security update)

## CI/CD - Continuous Integration / Continuous Delivery

Průběžné přispívání a průběžné vydávání

1. Přístup k plánování a odevzdávání úkolů
    - Příspěvěk nesmí paralyzovat celý systém
    - Příspěvěk musí být reverzibilní - nesmí zničit data kdyby bylo potřeba zacouvat v historii
    - Nové funkcionality se objevují ihned, samostatně a nečekají na slavnostní vydání další verze (většinou interně, někdy i veřejně - např.: Spotify)

2. Sada akcí provedených nad commitem (bodem v historii)
    - která přeloží a zkontroluje program, spustí testy, změří kvalitu (kódu i performance), nainstaluje program do reaálného prostředí, upozorní relevantní osoby...
    - Ne všichni vývojáři musí mít zprovozněné všechny akce lokálně a přesto mohou přispět do některých částí společného kódu. Například backend vs. frontend nebo infrastructure-as-a-code.

## CI/CD - Výhody
- V každém okamžiku (bodu historie) mám spustitelný program, který může použít i nevývojář (tester, analytik, zákazník)
- Každý commit projde překladem, automatickými testy, je podroben kvalitativním SW metrikám
- Každý commit* je nasazen do reálného prostředí
- CI/CD plně popisuje jak se program překládá a spouští. 
  - Žádné neaktuální slovní popisy, ale skutečné spouštění.
- Možnost dodávat funkcionalitu velmi brzo.

> \* nebo na požádání, nebo dle kritérií (tag, komentář, cron)

## Pipeline (CD)

- *Pipeline* - Definice vzájemně souvisejících úkonů spouštěných v GitLabu (či jiném integračním systému: bitbucket, github, jenkins)
- *Job* - miniaturní program jehož spuštění je prezetováno uceleně
- *Stage* - Fáze reprezentující nějaký význam práce s kódem. 
  - Podmíněné spuštění 
    - předchozí fáze doběhla, 
    - název větve splňuje nějaký výraz (regex)
    - prostředí splňuje nějaké podmínky

### Př.: Pipeline pro každou větev
1. Ukázat .gitlab-ci.yml
2. Ukázat Pipelines v gitlabu
3. Vysvětlit Stages a Jobs
4. Uázat výstup pipeline/jobu a korelaci s .gitlab-ci.yml

## Pipeline - Příklad Build
Pipeline pro každou větev
- Použít docker `image: mcr.microsoft.com/openjdk/jdk:21-ubuntu` pro všechno
- Přidat variables a cache

```yaml
variables:
  MAVEN_OPTS: "-Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository"

cache:
  key: "M2"
  paths:
    - .m2/repository
```
- Úkol (job) *build-job* bez testů `- ./mvnw package -DskipTests`

## Variables

Settings -> CI/CD -> variables

- *Protected variables* - ochrana proti (ne)chtěnému zvěřejnění hesel nováčky. Stejně jako protected branches.

### Př. Založení a použití variable
- Použít proměnnou `KIV_ZPP_HELLO` v jobu *lint-test-job*

```yaml
lint-test-job:
  stage: test
  script:
    - echo "Linting code... $KIV_ZPP_HELLO"
```
- Commit, Push
- Ukázat výsledek
- Udělat gitlab CI/CD variable `KIV_ZPP_HELLO`
- Pustit job znovu
- Ukázat výsledek

## Artifacts
- Soubory jakožto výsledek překladu v rámci jobu
- Nástroj mezi-fázové komunikace
  - Inter-stages communication

### Př. Uložení artefaktu v buildu

- Do build jobu přidat sekci `artifacts:`

```yaml
artifacts:
  paths:
    - target/workshop-*.jar
```
- Commit, push
- Ukázat výsledek a artefakty

## Artifacts - příklad použití v následující fázi

- Do *lint-test-job* přidat závislot na *build-job*

```yaml
dependencies:
  - build-job
```
- Do *lint-test-job* přidat skript, který ukáže detaily souboru 

```yaml
script:
  - ls -la ./target
```
- Commit, push
- Ukázat výsledek běhu jobu

## Pipeline - Příklad Testy a reporty
Spuštění testů a uložení výsledků
- Testy v jobu *unit-test-job*

```yaml
script:
  - echo "Running unit tests..."
  - ./mvnw test
```
- Test report v jobu *unit-test-job*

```yaml
artifacts:
  reports:
    junit:
      - target/surefire-reports/TEST-*.xml
```
- Commit, push
- Ukázat výsledek běhu - hlavně test reporty

## Pipeline - Příklad spadlé Testy
Když test spadne pipeline nedoběhne

- Vyrobit padající test

```java
  @Test
	void dummy() {
		throw new IllegalArgumentException("KIV/ZPP demo");
	}
```
- Commit, push
- Ukázat výsledek běhu 
  - Build je zelený protože nepouští test
  - Test spadnul
- Opravit test

```java
  @Test
	void dummy() {
		assertEquals(2, 1+1);
	}
```
- Commit, push
- Ukázat výsledek běhu: zelená

## Pipeline - podmíněý běh

Některé úkoly (job) chceme spouštět pouze ve speciálních případech. Například nad hlavní integrační větví.

### Příklad

- Udělat feature branch jako martin.simek@unicorn.com
- Do jobu *deploy-job* přidat podmínku na větev

```yaml
  only:
    - master
```
- Commit, push
- Ukázat výsledek pipeline: chybí build stage
- Udělat merge request do masteru
- Ukázat jak se spustila pipeline
- Přijmout merge request uřivatelem cima.m@seznam.cz
- Ukázat pipeline

## Deployment

Pipeline s fázemi a joby které nasazují přeložený program.

Nasazení je velmi technologicky specifické 
a často svázáno s nějakým nasazovacím nástrojem,
který za sebou mívá další systém pipeline.

- FTP, SSH
  - Obecný virtuální počítač
- Source to image - cizí pipeline znovu vybuildí kód a výsledek přemění na docker image
  - Openshift
  - Heroku
- "Local git" - Azure app service
  - Komitnu kód do jiného repozitáře (jiný remote)
  - Azure to přeloží zabalí nasadí spustí.

Nasazení vyžaduje přistupové údaje -> *Protected variables*

## Deployment - příklad Azure

- Zavést proměnou `AZURE_GIT_CREDENTIALS`

```yaml
variables:
  AZURE_GIT_CREDENTIALS: azure-git-credentials.txt
```
- Upravit *deploy-job*, aby vypadal následovně

```yaml
deploy-job:
  image: bitnami/git
  environment: production 
  stage: deploy
  script:
    - echo "Deploying application..."
    - echo $AZURE_GIT_CREDENTIALS
    - echo "$AZURE_GIT_PASSWORD" > "${AZURE_GIT_CREDENTIALS}"
    - git config --local user.email "cima.m@seznam.cz"
    - git config --local user.name "Gitlab (Martin Šimek)"
    - git config credential.helper "store --file=$AZURE_GIT_CREDENTIALS"
    - git remote add azure $AZURE_GIT_URL
    - git checkout $CI_COMMIT_REF_NAME 
    - git push azure HEAD:master
    - echo "Application successfully deployed."
```

## Závěr

- *Gitlab* - Spolupráce vyžaduje nástroje
- *Modely větvení* - Spolupráce více lidí potřebuje pravidla
- *Větve* - Vývoj probíhá ve více fázích - potřeba paralelních úložišť
- *Pipelines* - Automatizace rutiních úkolů: Kvalita, testy, dokumentace
- *Release on demand* - kdykoliv na počkání vydám a nasadím novou verzi.

# Díky za pozornost