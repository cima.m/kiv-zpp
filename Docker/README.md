# Docker
_Ing. Martin Šimek_

KIV/ZPP 4\. 3. 2025

# https://gitlab.com/cima.m/kiv-zpp/-/tree/main/Docker

## Obsah

- Teorie
- Docker Image
- Docker registry
- Kontejner
- Docker Daemon
- Dockerfile
- Mapování portů
- Mapování souborů
- Ostatní – Kubernetes/Openshift


## Motivace

- *Izolace procesu* – Od systému souborů, Od sebe vzájemně.
- *Update* – vymění se všechno najednou. I se systémem.
- *Distribuce* – Místo složitého návodu co nainstalovat a jak to nakonfigurovat, distribuuji celý systém – Image.
- Heterogenita – spuštění programu, který by volně běžící nenašel potřebné závislosti
  -  Spustit Debianí program na Centosu **jde**.
- *Konflikty verzí* – Na jednom systému může běžet více programů, které mají protichůdné závislosti


# Teorie
Program, Proces, Prostředí, File systém

## Program vs. Proces – program

- *Program* = Posloupnost instrukcí
- Serializované v souboru dle pravidel dané technologie
  - Vstupní bod
  - Definice závislostí (knihovny, obecné soubory)

![Proces](img/Program.png)


## Program vs. Proces – proces

- *Proces* = instance programu
- instancí může být více
- vzájemně oddělené paměťové prostory

![Proces](img/Proces.png)

## Prostředí

- Běh programu mohou ovlivnit
  - Vstupní parametry
  - Proměnné prostředí
  - Soubory dostupné procesu

- Tentýž program spuštěný ve dvou různých prostředích může vrátit dva různé výsledky
- Absence očekávaných souborů (knihoven) může způsobit pád programu
  - Stejně tak špatná verze knihoven

- Dva různé programy mohou záviset na stejné knihovně, ale v různé verzi
  - Řešení: Vendoring – programy se distribuují s knihovnami ve stejném adresáři

## Procesy vs. Prostředí

![Program and two processes](img/Program-all.png)

## Chroot
Change root

- Soubory s absolutní cestou nebo místa obvyklá pro hledání knihoven lze procesu pozměnit spuštěním pomocí chroot
- Spustí proces tak, že si program bude myslet, že adresář / (kořen souborového systému) je ve vámi definovaném adresáři
  - Systém v systému
  - Kernel a systémové služby zůstávají stejné jako bez chrootu – mění se pouze filesystem

- Příkaz z roku 1979 [ https://en.wikipedia.org/wiki/Chroot ]
- První izolace – kontejnerizace procesu

- Nevýhody
  - Bordel na disku
  - Příliš mnoho I-nodů v tabulce souborů 
    - (řešení pomocí FileFS – defacto zip bez komprese)
  - Složitá příprava nového rootu (debootstrap… viz https://help.ubuntu.com/community/BasicChroot )

# Docker image
Formalizovaný způsob jak distribuovat souborový systém

## Docker image - Vrstvy
Sada tar.gz souborů – tzv. Vrstev
- Každá vrstva obsahuje adresářovou strukturu od kořene
- Vrstva se odkazuje na svého rodiče – řetězení
- Rozbaluje se od nejvzdálenějších rodičů
- Každý potomek může přidat, přepsat nebo smazat soubory svých rodičů
- „Negativní“ soubory
- Změny autributů

![Proces](img/docker-image.png)

## Docker image - Hlavní přínos

Transparentnost a formalizace zacházení s obrazy přinesla zásadní průlom v kontejnerizaci

- Komunitním přínosem pak byla tvorba univerzálních obrazů
- Business model firmy Docker je momentálně nejasný
  - Hosting hosting Obrazů, Hosting kontejnerů
  - Změna obchodních podmínek
  - Alternativní klientské nástroje
  - Alternativní kontejnery

## Docker image - Demo ls

```bash
docker image ls
```

![Proces](img/docker_image_ls.png)


## Docker image - Demo pull
https://hub.docker.com/_/python

```bash
docker image pull python:3.12-bookworm
```

![Proces](img/docker_image_pull.png)


# Docker Image registry
Úložiště obrazů Docker

## Docker Image – registry

Úložiště obrazů Docker

- Souborové úložiště s databázovým (REST API) přístupem
- jméno:verze
  - java:15
  - python:3.10-buster
  - python:3.10-alpine

- Froky nebo skupiny
  - xlhybrids/img:v0.5.6

- Soukromé registry
  - mcr.microsoft.com/oryx/nodejs:10.13.0-alpine
  - nexus.mycompany.com:5443/img:v0.5.6-poisoned

- *Autentizace* – pověření se ukládá (do souboru, do správce pověření, do RAM), posílá se s každým REST dotazem

## Docker Image registry - demo private
```bash
docker login docker.ume-t.entsoe.eu
cat .docker/config.json
```

![Proces](img/docker_login_umet.png)

## Docker Image registry - demo public

```bash
docker login
cat .docker/config.json
```

![Proces](img/docker_login.png)

# Kontejner 
Virtuální realita pro proces

## Kontejner 
- Jasně ohraničené prostředí, ve kterém proces běží
- Pojmenovaný (cíleně či náhodně)
- Má ID
- V hostitelském systému lze jednoznačně identifikovat procesy běžící uvnitř kontejneru
- Kontejner je vázaný na hostitele
- Vstupní proces má PID=0

## Hlavní způsoby použití kontejneru

- Služba
  - Kontejner běží dlouhodobě na pozadí a vystavuje internetové porty

- Jednorázové lokální spuštění
  - Spuštění progrmau, který není pro hostitelský počítač k dispozici
  - Stejně jako shell script – pustím, běží, skončí
  - Spuštění během CI/CD

- Interaktivní
  - Kontejner propojí svoje I/O s interaktivním terminálem uživatele na hostitelském počítači (můžu psát)

- Průběžně běžící lokální spuštění
  - Spustím kontejner s nekončícím procesem v interaktivním módu
  - Docker exec + jméno kontejneru = spuštění příkazu v kontejneru nikoliv v hostitelském systému

## Kontejner - demo ps

```bash
docker ps
docker ps -a
```

![Proces](img/docker_ps.png)
![Proces](img/docker_ps_a.png)

## Kontejner - demo interaktivní #1
V jedné konzoli spustíme následující
```bash
python --version
python3 --version
docker run --name interactive_mogwai --rm -i -t python:3.12-bookworm bash
python --version
```

![Proces](img/docker_run.png)

## Kontejner - demo interaktivní #2
V další konzoli potom

```bash
docker run --name old_mogwai --rm -i -t python:3.10-bullseye bash
python --version
```

nakonec ve třetí konzoli

```bash
docker exec -it interactive_mogwai python --version
docker exec -it old_mogwai python --version
```
![Proces](img/docker_exec.png)

# Docker daemon
Správce kontejnerů


## Docker daemon

-Sytémová služba
  - Spravuje kontejnery
  - Systemd
  - Konfigurace pomocí „drop-in“ souborů
- Server
  - Poslouchá příkazy docker <něco>
  - Port 2375 (2376 šifrovaně)
  - Lze mu poslat příkazy vzdáleně (nejen z localhostu)
    - Docker for windows – ve virtuálce běží linux, na windowsech je docker.exe, příkazy jdou „po síti“
- Klient
  - Připojuje se na server a posílá dotazy
  - Proměnná prostředí, Konfigurační soubor
    - DOCKER_HOST=myRemote.neco.cz:2376
    - ~/.docker/config.json

![Docker Daemon](img/docker_daemon.png)

## Docker daemon - systemctl

```bash
sudo systemctl status docker
```

![systemctl](img/systemctl.png)

## Docker daemon - config proxy
Proxy client – https://docs.docker.com/network/proxy/

![docker config proxy](img/docker-config-proxy.png)


# Dockerfile
Předpis pro výrobu docker image

## Dockerfile

- Soubor s názvem Dockerfile
- Kontext – adresář se soubory, kde se kompilace pouští
- Sekvence příkazů jejichž důsledkem je změna prostředí
  - Proměnné prostředí
  - Kopírování souborů
  - Spouštění libovolných příkazů
    - Skripty
    - Apt-get install
    - Yum install

- Vlastní „kompilace“ probíhá uvnitř kontejneru
  - Změny se zaznamenávají s každou instrukcí Dockerfilu
    - 1 instrukce = 1 vrstva
      - Ne všechny vrstvy jsou explicitně pojmenované a viditelné

## Dockerfile - instrukce

- **FROM** – odkaz na rodičovskou vrstvu, na které stavět. Zároveň je to obraz pro kompilační kontejner.
- **COPY/ADD** – přidání souborů do výsledného obrazu
- **ENV** – nastavení proměnných prostředí, které obraz nastaví v kontejneru při spuštění
- **RUN** – spuštění libovolného příkazu během kompilace uvnitř kompilačního kontejneru
- **WORKDIR** – pracovní adresář, kde v kontejneru poběží proces při vytvoření kontejneru
- **ENTRYPOINT** – vstupní bod. Jakou aplikaci spustit když kontejner naběhne. Většinou shell/bash.
  - **CMD** – parametry vstupního bodu. Jelikož vstupní bod bývá shell/bash tak CMD je nějaký náš program

- **ARG** – možnost docker file parametrizovat. Např. verzí nějaké instalované aplikace (java, python, …)
- **EXPOSE** – porty běžícího kontejneru zpřístupněné z hostitelského systému. Mapování portů vizte dále.
  - Kontejner jinak nezpřístupňuje žádné porty na kterých poslouchají běžící procesy uvnitř
- **VOLUME** – Zpřístupnění souborů mimo image. Oddělení dat od programu. Perzistence.


## Dockerfile - Demo

```Dockerfile
FROM python:3.12-bookworm
WORKDIR /opt/myHello/
COPY myLicense.txt .
RUN python -help > python-help.txt
ADD hello.py .
ENV MY_NAME=Gizmo
CMD python hello.py "$MY_NAME"
```


Překlad (v adresáři Docker/image-hello)
```bash
docker build -t cimam/cima:1.0 .
```

![docker build](img/docker_build.png)

## Dockerfile - Demo run

```bash
docker run -ti cimam/cima:1.0
docker run -ti -e MY_NAME=Cima cimam/cima:1.0
docker run -ti cimam/cima:1.0 bash
```

![docker run](img/docker_run_env.png)

![docker run bash](img/docker_run_explore.png)


# Mapování portů

## Mapování portů

- Proces v kontejneru může poslouchat na TCP/UDP portu
- Porty nejsou v hostitelském systému dostupné
  - Lze je vystavit (expose) – budou vidět mezi kontejnery
  - Lze je mapovat – Budou vidět v hostitelském systému

- Subnet – virtuální síť mezi kontejnery
  - Izolovaná od hostitelského počítače
  - Více vzájemně oddělených sítí

![docker subnet ](img/docker-subnet.png)

## Mapování portů - demo

Vlastní Docker image s triviálním webserverem

```Dockerfile
FROM python:3.12-bookworm
WORKDIR /opt/myHello/
COPY myLicense.txt .
RUN python -help > python-help.txt
ADD hello.py .
ENV MY_NAME=Gizmo
CMD python -m http.server 8086
EXPOSE 8086
```

Překlad a spuštění (v adresáři Docker/image-hello)
```bash
docker build -f Dockerfile.server -t cimam/cima:1.0 .
docker run -ti -p 80:8086 cimam/cima:1.0
```

![docker run server with port mapping](img/docker_run_server.png)

![docker run server in browser](img/simple-server-browser.png)


# Mapování souborů



## Mapování souborů - Oddělení programu a dat

- Docker image má obsahovat pouze program a nezbytné zdroje (písmo, CSS, GUI obrázky)
  - Nikdy data – data jsou předmětem externích zdrojů (soubory, databáze, jiné servery)
- Persistence dat – výsledek běhu může být soubor a jeho zápis na filesystém je legitimní
- Vstupní data – data potřebná k výpočtu jsou poskytována jako soubory ve filesystému
  - Statická data webobvá prezentace
  - Jednorázový výpočet 
    - data science
    - Machine learing
    - Video processing

## Mapování souborů - Mapování do docker kontejneru

- Procesům uvnitř kontejneru může být zpřístupněn zvolený adresář
- Persistentní úložitě jako BLOB – kompaktní soubor (obsahující filesystém) připojovaný kontejneru jako R/W
  - Aplikace v kontejneru si může uložit stav

> Pozn.: Stavovost aplikace a hlavně její persistence do souboru by se nikdy neměla vyskytovat u webových aplikací *)

---
*) Aby aplikace mohla škálovat nikdy by si neměla ukládat stav mezi dotazy. Ani do RAM. A už vůbec ne do souboru. Pokud dotazy potřebují kontext a nelze jej posílat s každým dotazem, měly by tyto session být uloženy v databázi (NOSQL). 

Např. složité query se má jedním dotazem uložit, pšt se vrátí identifikátor uložené query a pootm dalším dotazem si začnu říkaz o výsledek. Pokud by query mělo stránkovat, vrací se na první query paging identifikátor a dotazy na další stránky se posílají s query paging identifikátorem.


## Mapování souborů - demo

```bash
docker run --mount type=bind,source=/mnt/c/src-orig/KIV-ZPP/Docker/image-hello,target=/opt/myHello/orig -p 80:8686 -ti cimam/cima:1.0
```

![docker run mount bind folder](img/docker_run_mount.png)

# Ostatní
Kubernetes/Openshift

## Docker Compose

- Formální popis, YAML (JSON) 
  - docker-compose.yml
- Popis Kontejnerů, Volume, Sítí
- Běh na pozadí jako služba
- Namespace – jmenný prostor
  - každý YAML = namespace 
  - Možnost referencovat napříč jmennými prostory
  - Oddělení restartování


docker-compose.yml
```yaml
version: "3.9"

services:
  serving_mogwai:
    image: cimam/cima:1.0
    hostname: serving-mogwai
    ports:
      - "80:8086"
    volumes:
      - /mnt/c/src-orig/KIV-ZPP/Docker/image-hello:/opt/myHello/orig
    environment:
      - ALLOW_ANONYMOUS_LOGIN=yes
  sleeping_mogwai:
    image: cimam/cima:1.0
    hostname: sleeping-mogwai
    environment:
      - MY_NAME=sleeping-mogwai
    entrypoint: ["sleep", "infinity"]
```
![docker compose up -d](img/docker_compose_up.png)

## Docker Compose - demo
Z jednoho kontejneru přistoupím do druhého a stáhnu soubor

```bash
docker exec -ti docker-sleeping_mogwai-1 bash
wget http://serving-mogwai/orig/myLicense.txt
wget http://serving-mogwai:8086/orig/myLicense.txt
```

![docker exec in compose](img/docker_exec_compose.png)

## Kubernetes
Cluster a Orchestrace

- *Sdílení prostředků* – Jeden počítač se plně nevyužije, jeden počítač nestačí
- *Izolace use casů* – Více use casů na společném HW
- *Transparentnost* – Různé počítače transparentně pro jeden use case
  - Mapování M:N – M usecasů na N počítačích

- *Služba* – Definice toho jaký kontejner a kde má běžet
  - *Replika* – Služba může běžet ve více instancích (škálování)
  - *Pod* – kontext kontejneru dané služby na konkrétním počítači ~ kontejner
- *Sítě* – Definice privátní sítě, na které se budou vidět/potkávat služby
- *Úložiště* – definice pracovních prostorů, persistentních úložišť
- *Load balancer* (ingress) – reverse proxy směrující provoz na všechny instance dané služby
Možnost bezvýpadkového upgradu


## Openshift
- Nadstavba nad Kubernetem
- Projekty – oddělení usecasů/projektů
- CI/CD – source-to-image stáhne repo, vyrobí docker image, nasadí
- Docker image registry -  per projekt
- Práce s doménami

- Výrazně jednodušší správa než holého Kubernetu
- Podpora od Red Hatu

## Kubernetes/Openshift

![Kubernetes subnets](img/kubernetes-subnet.png)

## Reference

- Docker image, https://docs.docker.com/engine/reference/commandline/image/ 
- Docker ps, https://docs.docker.com/engine/reference/commandline/ps/ 
- Docker run, https://docs.docker.com/engine/reference/commandline/run/ 
- Dockerfile, https://docs.docker.com/engine/reference/builder/ 
- Mapování portů, https://docs.docker.com/config/containers/container-networking/ 
- Mapování souborů, https://docs.docker.com/storage/bind-mounts/ 
