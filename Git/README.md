# Git a code review

_Ing. Martin Šimek_

KIV/ZPP 18. 2. 2025

Jak formalizovat a automatizovat proces vývoje softwaru, 
aby se vývojář mohl soustředit na programování 
a bylo ponětí, co ve které verzi programu vlastně je.

## Obsah

- Motivace – proč verzovat
- Co je to verzovací systém
- Historie, Větvení, Slučování, Rebase
- Konflikty - řešení a prevence
- Branchovací modely - Gitflow, Gitlabflow
- Protected branches v gitlabu
- Code Review
- Co je to CI/CD a proč ho mít
- Pipelines, Variables, Secrets
- Build reporty (unit testy, SW metriky)
- Deployment

## Motivace - verzování (Git)

- Centrální úložiště - zdroj pravdy
  - Všichni pracují na společném díle
- Historie změn (s popisem a metadaty)
  - Každý příspěvek nese stručný komentář co obsahuje a odkaz na detaily + *Autora*
  - *Persistentní undo* - Experimentuji a po nalezení chyby/řešní se můžu vrátit do pěkného stavu a napsat čistý kód
- Verze programu
  - konkrétní bod v historii vývoje ze kterého se vydala verze 1.0.0, 1.0.1, ...
- *Větve* - Paralelní historie
  - Příspěvek trvá více dní než se má přidat do hlavního proudu
  - Vydané verze potřebují opravná vyrání (patch, hotfix, security update)

## Co je to verzovací systém

Version Control System (VCS)

- Doplněk souborového systému umožňující zaznamenávat historii změn
  - Kompletní vyvolání stavu souboru v daném zaznamenaném bodě (commit)
- Různé druhy dat vyžadují různé způsoby ukládání historie
  - Inkrementální vs. absolutní
  - textové vs. binární

### Vizuální data 
- vestavěné v konkrétním programu
  - Autocad ( https://www.youtube.com/watch?v=L-YYoEqlvZY )
  - Word/Excel
  - PCB design ( https://resources.altium.com/p/best-practices-hardware-version-control-systems )

### Poučení
- Vybírat verzovací systém úměrně druhu dat/vývoje

## Verzovací systémy pro vývoj SW
- GIT - industry standard
  - CVS, SVN, Mercurial, TFS
- *Repozitář* - adresář s projektem a historií
  - *Remote* - Centrální úložiště, zdrojpravdy
  - *Local* - lokální kopie na vývojářském počítači
- *Commit* - revize, verze, bod v historii repozitáře
  - Seznam odebraných a přidaných řádků oproti předchozí verzi

### Příklad
Vytvoření a klonování repozitáře
1. Vyrobit repozitář na gitlabu - s šablonou *Spring* ať už tam něco je
2. Získat adresu na vyklonování (HTTPS, SSH necháme na čtenáři jako domácí cvičení)
3. Klonovat
4. Ukázat adresář
5. Ukázat, že tam je nastaven remote.

## Reprezentace historie
- *Blockchain* - konkrétní bod v historii je dán součtem inkrementů (rozdílů) od počátku do daného bodu.
- *Diff* - pro každý soubor, čísla řádků + kontext, odebrané řádky, přidané řádky
  - Kontext umožňuje zasadit diff i když nesedí čísla řádků
- *Pack* - fyzický soubor v adresáři .git sdružující několik commitů. Transakční jednotka při přenosu a vyhodnocování.
- *Ekvivalence* - dva soubory mohou znamenat totéž i když některé řádky jsou zpřeházené
  - XML, JSON, YAML, Programovací jazyky
    - Stromová struktura, refactoring

### Př.: Commit - normální příspěvek
1. Přidat něco do souboru
2. smazat něco ze souboru
3. Commitnout to
4. Ukázat diff

## Paralelní Historie
- *Větev* (*Branch*) - rozdvojení historie. Pomyslná kopie v jiném adresáři s vlastními změnami.
  - vydávání verzí programu
  - koooperace více lidí
  - Experimenty, hotfixy, ...
- *Fork* - kopie repozitáře která si tvoří vlastní historii, vlstní větve a sdílí společnou minulost.
  - Nezávislý vývoj jinou firmou včetně CI/CD a release branchí

> Př. Feature branch
> 1. Vyrobit větěv z HEADu *develop* větve
> 2. Komitnout do *develop* větve
> 3. Komitnout do nové větve
> 4. Pushnout obojí
> 5. Ukázat, že ve větvích je vidět různý obsah.

## Integrace příspěvků
- *Upstream*, *Integrační* větev např.: *master*, *main*, *dev*
- *Feature* branch - dočasná paralelní historie pro vývojáře
- *Merge* - přijetí feature větvě do integrační větve.
  - jeden vývojář zkontroluje co napsal ten druhý
  - Přimíchá dočasnou historii do té hlavní.
- *Rebase* - někdy je praktičtější vést historii jako jednoducohu linii
  - *Squash* - sloučení více komitů do jednoho
- *Protected branch* - větev, do které lze přidat pouze pokud máte práva
  - *main*, *release*...
  - většinou zodpovědnost senior developerů
  - "ochrana" před nováčky
  - Settings -> Repository -> Protected branches
  - Existují i protected tags.

### Poučení
- Způsob integrace volíme s ohledem na provoz programu a po domluvě s týmem.
- Neexistuje univerzální rada, který je ten správný.

## Merge request (CI)

- Malý příspěvěk 
  - krátké review
  - snadná kontrola
  - dobrá dekompozice kódu

- Myslíme to vážně -> brzy se příspěvěk dostane do produkce.
- Podmínky přijetí - Definition of done
  - Co musí být splněno (formátování, testy, přeložitelnost, dokumentace,...)
  - Některé kontroly zajistí automatizace v podobě Pipeline

### Př.: Review a merge
1. Ukázat upstream (develop). 
2. Udělat feature branch. 
3. Něco tam commitnout & pushnout.
4. Udělat review. 
5. Mergnout.

## Rebase - Příklad

Posunutí dočasné historii na vrchol hlavní tak, aby příspěvky (commity) tvořili jednu linii.

1. Ukázat upstream (develop)
2. Udělat feature branch níž než je HEAD developu/mainu/mastru
3. Něco tam commitnout
4. Ukázat lini historie a obsah adresáře
5. Udělat rebase
6. Ukázat lini historie a obsah adresáře

## Protected branches - Přiklad

Do některých větví smí přispívat (commitnout/mergnout) pouze uživatelé s vyššími právy.

1. Udělat branch release
2. Nastavit jí jako protected
3. Commitnout tam uživatelem, který nemá práva
4. Commitnout tam uživatelem, který má práva

## Konflikty
Konflikt sloučení vznikne pokud došlo v obou historiích ke změně na témže řádku.
Nelze rozhodnout, který příspěvěk má větší prioritu -> ruční rozhodnutí.

### Př.: Navození a řešení konfliktu
1. Udělat feature branch.
2. Změnit jden řádek v develop/main/maste. (Simulace paralelního příspěvku)
3. Změnit ten samý řádek ve feature branchi, ale jinak.
4. Udělat merge

## Konflikty - prevence

- Před integrací musí autor přijmout změny z upstreamu (občerstvení feature větve)
  - Tím se konflikt projeví jemu a on/a nejlépe ví co vybrat.
  - Pak už je zaintegrování do upstreamu bezkonfliktní a reviewer to zvládne.

### Co nám konflikty napovídají?
- Špatná dekompozice programu
- Špatná dělba práce

## Branchovací modely

- Kdy vytvářet větvě
  - Jak je pojmenovávat
- Co ty větvě vyjadřují a kam mergovat

### Git flow
https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
- Develop, feature, release, hotfix
- main/master

### Github flow
https://githubflow.github.io/
- pouze feature branche

### Gitlab flow
https://about.gitlab.com/topics/version-control/what-is-gitlab-flow/
- Branch-per-environment

## Závěr

- *Gitlab* - Spolupráce vyžaduje nástroje
- *Modely větvení* - Spolupráce více lidí potřebuje pravidla
- *Větve* - Vývoj probíhá ve více fázích - potřeba paralelních úložišť

# Díky za pozornost