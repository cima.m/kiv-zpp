# KIV/ZPP

[Sylabus KIV/ZPP](https://portal.zcu.cz/StagPortletsJSR168/CleanUrl?urlid=prohlizeni-predmet-sylabus&predmetZkrPrac=KIV&predmetZkrPred=ZPP&predmetRok=2023&predmetSemestr=LS)

Přednášky o moderních principech softwarového vývoje v praxi.
Lidé z praxe přednášejí pro studenty o tom co všechno se musí dělat kromě programování při vývoji softwaru.

## Git & CI/CD
> 20\. 2\. 2024

Jak používat Git a jeho nadstavby (demonstrováno na Gitlabu) pro přispívání do kódu a jeho automatické zpracování.

Více v samostatném [adresáři CI-CD](CI-CD/README.md)

## Docker
> 27\. 2. 2024

Co je to docker, co se sním dá dělat a k čemu se to dá využít. Výhody, omezení, použití.

Více v samostatném [adresáři Docker](Docker/README.md)

## Convert to presentation
```
npm install markdown-to-slides -g
cd CI-CD
markdown-to-slides README.md --document-mode --level 2 -o CI-CD.html
```

# Zdroje

https://github.com/kernoeb/docker-markdown-pdf