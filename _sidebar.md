# KIV/ZPP

[Sylabus KIV/ZPP](https://portal.zcu.cz/StagPortletsJSR168/CleanUrl?urlid=prohlizeni-predmet-sylabus&predmetZkrPrac=KIV&predmetZkrPred=ZPP&predmetRok=2023&predmetSemestr=LS)

Přednášky o moderních principech softwarového vývoje v praxi.
Lidé z praxe přednášejí pro studenty o tom co všechno se musí dělat kromě programování při vývoji softwaru.

## Git 
> 18\. 2\. 2025

Jak používat Git a barnchovací modely (modely větvení) pro správu zdrojového kódu a spolupráci v týmu.

Více v samostatném [adresáři Git](Git/README.md)

## CI/CD
> 25\. 2\. 2025

Jak používat nadstavby Gitu (demonstrováno na Gitlabu) pro přispívání do kódu a jeho automatické zpracování.

Více v samostatném [adresáři CI-CD](CI-CD/README.md)

## Docker
> 4\. 3. 2025

Co je to docker, co se sním dá dělat a k čemu se to dá využít. Výhody, omezení, použití.

Více v samostatném [adresáři Docker](Docker/README.md)
