#!/bin/bash

docker run --rm --name "docker-markdown-pdf" \
  -e "CHOWN_IDU=$(id -u)" -e "CHOWN_IDG=$(id -g)" -e "FILE_LOCATION=kiv-zpp-ci-cd.pdf" \
  -v "$(pwd)/resources:/resources" \
  -v "$(pwd)/CI-CD":/app/documentation/:ro \
  -v "$(pwd)":/tmp:rw \
  ghcr.io/kernoeb/docker-markdown-pdf:main